# README #

This project is an extension designed to run on the Simple:Press Wordpress Theme Platform.  

### How do I get set up? ###

This theme is not a normal WordPress theme and cannot be installed via the WordPress THEMES screen.
Instead, you should:

- Download the theme file from your receipt or from your dashboard
- Go to FORUM->Themes
- click on `Theme Uploader`
- Click on the `Choose File` button and select the zip file you downloaded earlier
- Click the `Upload Now` button
- Go back to the FORUM->Themes screen and activate the theme.


### Change Log  ###
-----------------------------------------------------------------------------------------
###### 2.1.6
- Fix: make button css more specific to make sure that it wont target things outside Simple:Press 

###### 2.1.5
- Fix: views and replies does not align correctly

###### 2.1.4
- Fix: Add support for SP_USE_UPLOAD_DIR for using sp-resources in uploads

###### 2.1.3
- Tweak: Adjust so its better laid out in the 20/20 theme.  10% margin on left and right to attempt to center the forum; max width of 80%.

###### 2.1.2
- Tweak: Use a better theme image

###### 2.1.1
- Tweak: Make sure that push notifications icons do not show up unless the user has filled out the keys in their user profiles.

###### 2.1.0
- New: Add template tags for canned replies
- New: Add template tags for push notifications

###### 2.0.0
- New: 6.0 compatibility

###### 1.0.0
- Initial release
